/**
* Main Controller (controller.js)
* contains controllers
* for all the pages in ionic template
*/

angular.module('etg.controllers', ['ionic'])

.controller('MenuCtrl',function($scope, $ionicPopup, $timeout, $state, $ionicHistory) {

	var LanguagePopup;
	
	$scope.SetLanguage = function() {
		LanguagePopup.close();
	};

	$scope.GetLanguage = function() {
		LanguagePopup = $ionicPopup.show({
		    title: 'CHOOSE YOUR LANGUAGE',
		    scope: $scope,
		    template: '<span class="flex1"><label class="item item-select flex2"><select class="txtCustomLocationField"><option>ENGLISH</option><option>SWAHILI</option></select></label><button class="button button-light btnCustomPopupBtn" ng-click="SetLanguage()"><i class="ion-chevron-right"></i></button></span>'
		});
	};

	$scope.GotoShop = function() {
		$state.go('menu.shop');
	}

	$scope.GotoCart = function() {
		$state.go('menu.cart');
	}

	$scope.GotoOrders = function() {
		$state.go('menu.orders');
	}

	$scope.GotoProfile = function() {
		$state.go('menu.myprofile');
	}
})

.controller('LoginCtrl',function($scope, $ionicPopup, $timeout, $state) {

	var LanguagePopup1;

	$scope.login = function() {
		$state.go('menu.shop');
	};

	$scope.SetLanguage = function() {
		LanguagePopup1.close();
	};

	LanguagePopup1 = $ionicPopup.show({
	    title: 'CHOOSE YOUR LANGUAGE',
	    scope: $scope,
	    template: '<span class="flex1"><label class="item item-select flex2"><select class="txtCustomLocationField"><option>ENGLISH</option><option>SWAHILI</option></select></label><button class="button button-light btnCustomPopupBtn" ng-click="SetLanguage()"><i class="ion-chevron-right"></i></button></span>'
	});

})

.controller('SignupCtrl',function($scope, $ionicPopup, $timeout, $state) {

	$scope.signup = function() {
		$state.go('menu.shop');
	};
})

.controller('ForgotPasswordCtrl',function($scope, $ionicPopup, $timeout, $state) {

	$scope.reset = function() {
		$state.go('login');
	};
})

.controller('ShopCtrl',function($scope, $ionicPopup, $timeout, $state) {

	var LocationPopup;

	$scope.GotoProducts = function() {
		$state.go('menu.products');
	};

	$scope.SetLocation = function() {
		LocationPopup.close();
	};

	$scope.GetLocation = function() {

		LocationPopup = $ionicPopup.show({
		    title: 'CHOOSE YOUR DELIVERY LOCATION',
		    scope: $scope,
		    template: '<span class="flex1"><label class="item item-select flex2"><select class="txtCustomLocationField"><option>Nairobi</option><option>Mombasa</option><option>Kisumu</option><option>Nakuru</option><option>Eldoret</option><option>Malindi</option></select></label><button class="button button-light btnCustomPopupBtn" ng-click="SetLocation()"><i class="ion-chevron-right"></i></button></span>'
		});
	};

	$scope.GetLocation();
})

.controller('ProductsCtrl',function($scope, $ionicPopup, $timeout, $state, $ionicHistory) {

	$scope.CatId = 1; //category id

	$scope.GoBack = function() {
	    $ionicHistory.goBack();
	};

	$scope.ProductsList =function() {
		$state.go('menu.productlist');
	};

	$scope.ShowDetail = function() {
		$state.go('menu.productdetail');
	};

	$scope.GotoCart =function() {
		$state.go('menu.cart');
	};

	$scope.AllItems = true;

	$scope.NoResults = false;

	$scope.ShowItems = function(Id) {
		if(Id == 1) {
			$scope.AllItems = true;
			$scope.NoResults = false;
		}
		else {
			$scope.NoResults = true;
			$scope.AllItems = false;
		}
	}
})

.controller('ProductListCtrl',function($scope, $ionicPopup, $timeout, $state, $ionicHistory) {

	$scope.CatId = 1;

	$scope.GoBack = function() {
	    $ionicHistory.goBack();
	};

	$scope.ProductsGrid =function() {
		$state.go('menu.products');
	};

	$scope.ShowDetail = function() {
		$state.go('menu.productdetail');
	};

	$scope.AllItems = true;

	$scope.NoResults = false;

	$scope.ShowItems = function(Id) {
		if(Id == 1) {
			$scope.AllItems = true;
			$scope.NoResults = false;
		}
		else {
			$scope.NoResults = true;
			$scope.AllItems = false;
		}
	}
})

.controller('ProductDetailCtrl',function($scope, $ionicPopup, $timeout, $state, $ionicHistory) {

	$scope.GoBack = function() {
	    $ionicHistory.goBack();
	};

	$scope.GotoCart =function() {
		$state.go('menu.cart');
	};
})

.controller('CartCtrl',function($scope, $ionicPopup, $timeout, $state) {

	$scope.GotoCheckout = function() {
		$state.go('menu.checkout');
	};
})

.controller('CheckoutCtrl',function($scope, $ionicPopup, $timeout, $ionicHistory, $state) {

	$scope.GoBack = function() {
	    $ionicHistory.goBack();
	};

	$scope.GotoOrderConfirmation = function() {
		$state.go('menu.orderconfirmation');
	};

	$scope.EditAddress = function() {
		$state.go('menu.editcheckout');
	};
})

.controller('EditCheckoutCtrl',function($scope, $ionicPopup, $timeout, $ionicHistory, $state) {

	$scope.GoBack = function() {
	    $ionicHistory.goBack();
	};

	$scope.GotoOrderConfirmation = function() {
		$state.go('menu.orderconfirmation');
	};
})

.controller('OrderConfirmationCtrl',function($scope, $ionicPopup, $timeout, $state) {

	$scope.GotoShop = function() {
		$state.go('menu.shop');
	};
})

.controller('OrdersCtrl',function($scope, $ionicPopup, $timeout, $state) {

	$scope.ShowOrderDetail = function() {
		$state.go('menu.orderdetail');
	};
})

.controller('OrderDetailCtrl',function($scope, $ionicPopup, $timeout, $ionicHistory, $state) {

	$scope.GoBack = function() {
	    $ionicHistory.goBack();
	};

	$scope.CancelOrder = function() {
		$state.go('menu.orders');
	};
})

.controller('ProfileCtrl',function($scope, $ionicPopup, $timeout, $state) {

	$scope.EditProfile = function() {
		$state.go('menu.editprofile');
	};
})

.controller('EditProfileCtrl',function($scope, $ionicPopup, $timeout, $state, $ionicHistory) {

	$scope.GoBack = function() {
		$ionicHistory.goBack();
	};

	$scope.UpdateProfile = function() {
		$state.go('menu.myprofile');
	};
})
