/**
* Routes Controller (routes.js)
* contains paths and states
* for all the pages in ionic template
* to add new view the revelant 
* controller should be added.
*/

angular.module('etg.routes', ['ionic'])

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('menu', {
    url: "/menu",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'MenuCtrl'
  })

  .state('login', {
    url: "/login",
    templateUrl: "templates/login.html",
    controller: 'LoginCtrl'
  })

  .state('signup', {
    url: "/signup",
    templateUrl: "templates/signup.html",
    controller: 'SignupCtrl'
  })

  .state('forgotpassword', {
    url: "/forgotpassword",
    templateUrl: "templates/forgotpassword.html",
    controller: 'ForgotPasswordCtrl'
  })


  .state('menu.shop', {
    url: "/shop",
    views: {
      'tab-shop': {
        templateUrl: "templates/shop.html",
        controller: 'ShopCtrl'
      }
    }
  })

  .state('menu.products', {
    url: "/products",
    views: {
      'tab-shop': {
        templateUrl: "templates/products.html",
        controller: 'ProductsCtrl'
      }
    }
  })

  .state('menu.productlist', {
    url: "/productlist",
    views: {
      'tab-shop': {
        templateUrl: "templates/productlist.html",
        controller: 'ProductListCtrl'
      }
    }
  })

  .state('menu.productdetail', {
    url: "/productdetail",
    views: {
      'tab-shop': {
        templateUrl: "templates/productdetail.html",
        controller: 'ProductDetailCtrl'
      }
    }
  })

  .state('menu.orders', {
    url: "/orders",
    views: {
      'tab-orders': {
        templateUrl: "templates/orders.html",
        controller: 'OrdersCtrl'
      }
    }
  })

  .state('menu.orderdetail', {
    url: "/orderdetail",
    views: {
      'tab-orders': {
        templateUrl: "templates/orderdetail.html",
        controller: 'OrderDetailCtrl'
      }
    }
  })

  .state('menu.myprofile', {
    url: "/myprofile",
    views: {
      'tab-myprofile': {
        templateUrl: "templates/myprofile.html",
        controller: 'ProfileCtrl'
      }
    }
  })

  .state('menu.editprofile', {
    url: "/editprofile",
    views: {
      'tab-myprofile': {
        templateUrl: "templates/editprofile.html",
        controller: 'EditProfileCtrl'
      }
    }
  })

  .state('menu.cart', {
    url: "/cart",
    views: {
      'tab-cart': {
        templateUrl: "templates/cart.html",
        controller: 'CartCtrl'
      }
    }
  })

  .state('menu.checkout', {
    url: "/checkout",
    views: {
      'tab-cart': {
        templateUrl: "templates/checkout.html",
        controller: 'CheckoutCtrl'
      }
    }
  })

  .state('menu.editcheckout', {
    url: "/editcheckout",
    views: {
      'tab-cart': {
        templateUrl: "templates/editcheckout.html",
        controller: 'EditCheckoutCtrl'
      }
    }
  })

  .state('menu.orderconfirmation', {
    url: "/orderconfirmation",
    views: {
      'tab-cart': {
        templateUrl: "templates/orderconfirmation.html",
        controller: 'OrderConfirmationCtrl'
      }
    }
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');
});
